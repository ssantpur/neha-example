// stdlib functionality     
#include <iostream>
#include <fstream>

// ROOT functionality
#include <TFile.h>
#include <TH1D.h>
using namespace std;

int main(int argc, char **argv) {

  // Open the input file
  TFile *f_in = new TFile(argv[1]);

  // Collect the histogram from the file as a TH1D
  TH1D * hist = (TH1D*)f_in->Get("m_jj");

 // Initialize the outputfile object
 std::ofstream f_out(argv[2]);

 //---- First write the bin edges -----//
 // Get the bin width and number of bins from the histogram
 double bin_width = hist->GetBinWidth(3);	   // Relevant function: GetBinWidth()
 int n_bins = hist->GetNbinsX();            // Relevant function: GetNbinsX()

 // Loop through all the bins, and write the lower bin edge to the ouput file (with a space between subsequent bin edges)
 for(int iBin=1; iBin < n_bins+1; iBin++)
 {
   f_out<<hist->GetBinLowEdge(iBin)<<" ";                       // Relevant function: GetBinLowEdge()
 }

 // Add the bin width to the lower edge of the last bin to get the upper edge of the last bin, and write it to the text file
 f_out<<hist->GetBinLowEdge(n_bins)+bin_width<<endl;                       // Relevant function: GetBinLowEdge()

 // Now write the bin contents, again with a space between subsequent bin contents
 for(int iBin=1; iBin < n_bins+1; iBin++)
 {
   f_out<<hist->GetBinContent(iBin)<<" ";                        // Relevant function: GetBinContent()
 }

 f_out.close();
}
