// stdlib functionality
#include <iostream>
#include <fstream>
// ROOT functionality
#include <TFile.h>
#include <TH1F.h>
// ATLAS EDM functionality
#include "AsgTools/AnaToolHandle.h"
#include "JetCalibTools/IJetCalibrationTool.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include <xAODRootAccess/tools/TReturnCode.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "JetSelectionHelper/JetSelectionHelper.h"
//#include <PATInterfaces/CorrectionCode.h>
//#include <PATInterfaces/SystematicCode.h>
using namespace std;
asg::AnaToolHandle<IJetCalibrationTool> JetCalibrationTool_handle;

int main(int argc, char** argv) {
  // initialize the xAOD EDM
  xAOD::Init();

  //Jet calib tool set up
  JetCalibrationTool_handle.setTypeAndName("JetCalibrationTool/MyCalibrationTool");
  
  JetCalibrationTool_handle.setProperty("JetCollection","AntiKt4EMTopo"                                                  );
  JetCalibrationTool_handle.setProperty("ConfigFile"   ,"JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config");
  JetCalibrationTool_handle.setProperty("CalibSequence","JetArea_Residual_EtaJES_GSC_Smear"                              );
  JetCalibrationTool_handle.setProperty("CalibArea"    ,"00-04-82"                                                       );
  JetCalibrationTool_handle.setProperty("IsData"       ,false                                                            );
  
  JetCalibrationTool_handle.retrieve();
  
  // open the input file
  TString inputFilePath;
  inputFilePath = "/home/atlas/Bootcamp/Data/DAOD_EXOT27.17882744._000026.pool.root.1";
  if(argc >=2) inputFilePath = argv[1];
  xAOD::TEvent event;
  std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
  if(!iFile) return 1;
  event.readFrom( iFile.get() );

  //output file name
  TString outputfilename;
  outputfilename = "hist_output_jetcalibtool.root";
  if(argc >=3) outputfilename = argv[2];

  
  // get the number of events in the file to loop over
  Long64_t numEntries(-1);
  if(argc >= 4) numEntries  = std::atoi(argv[3]);
  if(numEntries == -1) numEntries = event.getEntries();
  std::cout << "Processing " << numEntries << " events" << std::endl;

  // for counting events
  unsigned count = 0;
  
  TH1F* njets = new TH1F("njets","njets",20,0,20);
  TH1F* nmu = new TH1F("nmu","nmu",20,0,20);
  TH1F* nel = new TH1F("nel","nel",20,0,20);
  TH1F* n_good_jets = new TH1F("n_good_jets","n_good_jets",20,0,20);
  
  TH1F* m_jj = new TH1F("m_jj","m_jj",25,0,500);

  //Read a config file
  //  ifstream file_to_read("/home/atlas/Bootcamp/neha-example/cut.config");
  ifstream file_to_read("../source/share/cut.config");
  
  // this information will be loaded to a map<string,double>                                   
  if(file_to_read.is_open() != 1 ){
    cout << "The cut.config file doesn't exist" << endl;
    exit(1);}
  map<string,double> config ;

  while(!file_to_read.eof()){
    string tmp_name ;
    double tmp_value ;
    file_to_read >> tmp_name >> tmp_value ;
    config[tmp_name] = tmp_value ;
    cout << tmp_name << " " <<   config[tmp_name] << endl;
  }

  //Create instance of Jet Selection Tool
  JetSelectionHelper jet_selector;
  
  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {

    // Load the event
    event.getEntry( i );

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    event.retrieve( ei, "EventInfo" );
    if(i%10000==0)std::cout << "Processed " << i << "entries " << std::endl;

    // retrieve the muon container from the event store
    const xAOD::MuonContainer* muons = nullptr;
    event.retrieve(muons, "Muons");

    std::vector<xAOD::Muon> mu_vec;
    mu_vec.clear();

    // loop through all of the jets and make selections with the helper
    for(const xAOD::Muon* mu : *muons) {
      // print the kinematics of each jet in the event
      mu_vec.push_back(*mu);
    }

    nmu->Fill((int)mu_vec.size());

    // retrieve the electron container from the event store
    const xAOD::ElectronContainer* electrons = nullptr;
    event.retrieve(electrons, "Electrons");

    std::vector<xAOD::Electron> el_vec;
    el_vec.clear();

    // loop through all of the electrons and make selections with the helper
    for(const xAOD::Electron* el : *electrons) {
      el_vec.push_back(*el);
    }

    nel->Fill((int)el_vec.size());

    
    
    // retrieve the jet container from the event store
    const xAOD::JetContainer* jets = nullptr;
    event.retrieve(jets, "AntiKt4EMTopoJets");

    std::vector<xAOD::Jet> jet_vec;
    jet_vec.clear();

    std::vector<xAOD::Jet> good_jet_vec;
    good_jet_vec.clear();

    // loop through all of the jets and make selections with the helper
    for(const xAOD::Jet* jet : *jets) {
      //if (jet_selector.isJetGood(jet)) 	good_jet_vec.push_back(*jet);
      // calibrate the jet
      xAOD::Jet *calibratedjet;
      JetCalibrationTool_handle->calibratedCopy(*jet,calibratedjet);
      jet_vec.push_back(*calibratedjet);
      if( jet_selector.isJetGood(calibratedjet) ){
	good_jet_vec.push_back(*calibratedjet);
      }
      delete calibratedjet;

      
      //      if(jet->pt()/1000.0 < config["jet_pt_cut"] || std::fabs(jet->eta())> config["jet_eta_cut"]) continue;
      //      std::cout << "Jet : pt=" << jet->pt() << "  eta=" << jet->eta() << "  phi=" << jet->phi() << "  m=" << jet->m() << std::endl;
      //jet_vec.push_back(*jet);
    }

    
    //Fill njets here
    njets->Fill((int)jet_vec.size());
    n_good_jets->Fill((int)good_jet_vec.size());
    //Fill m_jj here
    if((int)jet_vec.size()>1){
      m_jj->Fill( (jet_vec.at(0).p4()+jet_vec.at(1).p4()).M() /1000.0); //in GeV
    }
    
    // counter for the number of events analyzed thus far
    count += 1;

  } //event loop

  
  TFile *out = new TFile(outputfilename,"RECREATE");
  njets->Write();
  m_jj->Write();
  nmu->Write();
  n_good_jets->Write();
  nel->Write();
  out->Close();
    
  // exit from the main function cleanly
  return 0;
}

